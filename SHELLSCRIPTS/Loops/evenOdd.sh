#!/bin/bash
v=(1 2 3 4 5)
for i in ${v[*]}
do
res=`expr $i % 2`
if [ $res == 0 ]; then
  echo " $i is even"
else
  echo " $i is odd"
fi
done
echo " ------- EOS -------"

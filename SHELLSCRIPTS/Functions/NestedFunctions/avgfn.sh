#!/bin/bash
sum=0
ans=y
count=0

while [ $ans == 'y' ]
do   
     if [ $count -ne 0 ]; then
        read -p 'Do you want to contiue (Y/N)?:' ans
     fi
     if [ $ans == 'y' ]; then
        read -p' enter a number:' num
        sum=`expr $sum + $num`
        count=`expr $count + 1`
     fi
done
if [ $count -lt 2 ]; then
    echo " you need atleast 2 values for the average"
else     
    avg=`expr $sum / $count`
    echo " avg is $avg"
fi

#! /bin/bash

F=$1

if [ -e $F ]; then
   echo " files exits"
else
   echo " file doesn't exits"
fi

if [ -s $F ]; then
   echo " file size is non zero"
else
   echo " file size is zero"
fi

if [ -r $F ]; then
   echo " file has read permission"
else
   echo " file doesn't have read permission"
fi

if [ -w $F ]; then
  echo " file has write permission"
else
  echo " file doesn't have write permission"
fi

if [ -x $F ]; then
   echo " file has execute permission"
else
   echo " file has no execute permission"
fi

#!/bin/bash
var1=10
var2=0
echo "var1= $var1"
echo "var2= $var2"

echo "------ true -----"
if [ $var1 ]; then
	echo " 1. Got a true value"
fi

if [ $var1 == $var2 ]; then
	echo "2. Var1 is equals to var2 "
else
        echo "3. Var1 is not equals to var2 "
fi
echo " ------ end of script"



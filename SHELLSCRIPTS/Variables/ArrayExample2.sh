#! /bin/bash

Name=(orange Apple Grape)

echo " First Index: ${Name[0]}"
echo " Second Index: ${Name[1]}"
echo " All values using [@]: ${Name[@]}"
echo " All values using [*]: ${Name[*]}"


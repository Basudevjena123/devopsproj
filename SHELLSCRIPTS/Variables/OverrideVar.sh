#! /bin/bash

MyName='Divya'
echo " Myname is $MyName"
MyName='binnu'
echo " After overriding MyName"
echo " Myname is $MyName"

Array=(Orange Apple Banana)
echo " Array values are ${Array[*]}"
echo " Array[0] is ${Array[0]}"
Array[0]='Grape'
echo " After overriding First Index"
echo " Array[0] is ${Array[0]}"


